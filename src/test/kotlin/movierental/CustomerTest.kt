package movierental

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CustomerTest {

    @Test
    fun test() {
        val customer = Customer("Bob")
        customer.addRental(Rental(Movie("Jaws", Movie.REGULAR), 2))
        customer.addRental(Rental(Movie("Golden Eye", Movie.REGULAR), 3))
        customer.addRental(Rental(Movie("Short New", Movie.NEW_RELEASE), 1))
        customer.addRental(Rental(Movie("Long New", Movie.NEW_RELEASE), 2))
        customer.addRental(Rental(Movie("Bambi", Movie.CHILDRENS), 3))
        customer.addRental(Rental(Movie("Toy Story", Movie.CHILDRENS), 4))
        val expected = "" +
                "Rental Record for Bob\n" +
                "\tJaws\t2.0\n" +
                "\tGolden Eye\t3.5\n" +
                "\tShort New\t3.0\n" +
                "\tLong New\t6.0\n" +
                "\tBambi\t1.5\n" +
                "\tToy Story\t3.0\n" +
                "Amount owed is 19.0\n" +
                "You earned 7 frequent renter points"
        assertEquals(expected, customer.statement())
    }
}